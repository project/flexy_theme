(function ($) {
  //aboutpage tabs active
  var path = window.location.pathname.split("/").pop();
   if ( path == '' ) {
     path = 'about';
   }        
  var target = jQuery('ul.about-tabs-wrp li a[href="'+path+'"]');
   target.addClass('active');

  // Loading the animation js.  
  if ($( window ).width()>767) {
   new WOW().init();
  }

  //scroll to header
  $('a.wht').click(function() {
	  $('html, body').animate({
	    scrollTop: $("#navbar").offset().top
	  }, 1000)
	})

   //skillbar for page jquery 
    jQuery('.skillbar.page').each(function(){
        jQuery(this).find('.skillbar-bar').animate({
          width:jQuery(this).attr('data-percent')
        }, 2000); // 2 seconds
      });

  	//header stiky
  	window.onscroll = function() {myFunction()};
	var header = document.getElementById("navbar");
	var sticky = header.offsetTop;

	function myFunction() {
	  if (window.pageYOffset > sticky) {
	    header.classList.add("sticky");
	  } else {
	    header.classList.remove("sticky");
	  }
	}

	//skillbar jqeury
	 $(window).scroll(function() {
    var hT = $('#skill-bar-wrapper').offset().top,
        hH = $('#skill-bar-wrapper').outerHeight(),
        wH = $(window).height(),
        wS = $(this).scrollTop();
    if (wS > (hT+hH-1.4*wH)){
        jQuery(document).ready(function(){
            jQuery('.skillbar').each(function(){
                jQuery(this).find('.skillbar-bar').animate({
                    width:jQuery(this).attr('data-percent')
                }, 2000); // 1 seconds
            });
        });
    }
 });


}(jQuery));

